---
layout: default
title: Barghest - Sources
permalink: /sources
menu_entry: Sources
menu.index: 2
---

# Bitbucket repositories

{% for proj in site.data.projects %}
 * [{{ proj.name }}]({{ proj.repo | prepend: site.data.bitbucket.base }}){% endfor %}

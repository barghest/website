---
layout: default
title: Barghest
permalink: /
menu_entry: Home
menu.index: 0
---

# What is Barghest ?

Barghest is a binary static analysis tool suite targeting x86 and x86_64
architectures. Barghest is modular and open source.

The goal of Barghest is to provide multiple layers of tools to improve
software analysis in a variety of domains, including non exclusively
debugging, reverse engineering, research, and testing.

Barghest main strength comes from its ability to directly analyse the binary,
and from its ability to do static analysis.

Of course Barghest's approach still comes with several drawbacks, the main
ones being speed and resource consumption.

# Project Status

Barghest is still in active development.

# License

Barghest is licensed under the BSD 2 clauses license. More informations on
[opensource.org](http://opensource.org/licenses/BSD-2-Clause).
